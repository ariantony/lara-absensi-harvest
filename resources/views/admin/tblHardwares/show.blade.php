@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.tblHardware.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.tbl-hardwares.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.id') }}
                        </th>
                        <td>
                            {{ $tblHardware->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.id_fp') }}
                        </th>
                        <td>
                            {{ $tblHardware->id_fp }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.ip') }}
                        </th>
                        <td>
                            {{ $tblHardware->ip }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.c_key') }}
                        </th>
                        <td>
                            {{ $tblHardware->c_key }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.number_fp') }}
                        </th>
                        <td>
                            {{ $tblHardware->number_fp }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.desc') }}
                        </th>
                        <td>
                            {{ $tblHardware->desc }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.run_intervar') }}
                        </th>
                        <td>
                            {{ $tblHardware->run_intervar }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.brand') }}
                        </th>
                        <td>
                            {{ $tblHardware->brand }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.outlet') }}
                        </th>
                        <td>
                            {{ $tblHardware->outlet }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.machine') }}
                        </th>
                        <td>
                            {{ $tblHardware->machine }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tblHardware.fields.ftype') }}
                        </th>
                        <td>
                            {{ $tblHardware->ftype }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.tbl-hardwares.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection