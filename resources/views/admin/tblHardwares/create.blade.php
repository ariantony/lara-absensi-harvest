@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.tblHardware.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.tbl-hardwares.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="id_fp">{{ trans('cruds.tblHardware.fields.id_fp') }}</label>
                <input class="form-control {{ $errors->has('id_fp') ? 'is-invalid' : '' }}" type="number" name="id_fp" id="id_fp" value="{{ old('id_fp', '') }}" step="1">
                @if($errors->has('id_fp'))
                    <span class="text-danger">{{ $errors->first('id_fp') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tblHardware.fields.id_fp_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="ip">{{ trans('cruds.tblHardware.fields.ip') }}</label>
                <input class="form-control {{ $errors->has('ip') ? 'is-invalid' : '' }}" type="text" name="ip" id="ip" value="{{ old('ip', '') }}">
                @if($errors->has('ip'))
                    <span class="text-danger">{{ $errors->first('ip') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tblHardware.fields.ip_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="c_key">{{ trans('cruds.tblHardware.fields.c_key') }}</label>
                <input class="form-control {{ $errors->has('c_key') ? 'is-invalid' : '' }}" type="number" name="c_key" id="c_key" value="{{ old('c_key', '') }}" step="1">
                @if($errors->has('c_key'))
                    <span class="text-danger">{{ $errors->first('c_key') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tblHardware.fields.c_key_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="number_fp">{{ trans('cruds.tblHardware.fields.number_fp') }}</label>
                <input class="form-control {{ $errors->has('number_fp') ? 'is-invalid' : '' }}" type="text" name="number_fp" id="number_fp" value="{{ old('number_fp', '') }}">
                @if($errors->has('number_fp'))
                    <span class="text-danger">{{ $errors->first('number_fp') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tblHardware.fields.number_fp_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="desc">{{ trans('cruds.tblHardware.fields.desc') }}</label>
                <textarea class="form-control {{ $errors->has('desc') ? 'is-invalid' : '' }}" name="desc" id="desc">{{ old('desc') }}</textarea>
                @if($errors->has('desc'))
                    <span class="text-danger">{{ $errors->first('desc') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tblHardware.fields.desc_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="run_intervar">{{ trans('cruds.tblHardware.fields.run_intervar') }}</label>
                <input class="form-control {{ $errors->has('run_intervar') ? 'is-invalid' : '' }}" type="number" name="run_intervar" id="run_intervar" value="{{ old('run_intervar', '') }}" step="1">
                @if($errors->has('run_intervar'))
                    <span class="text-danger">{{ $errors->first('run_intervar') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tblHardware.fields.run_intervar_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="brand">{{ trans('cruds.tblHardware.fields.brand') }}</label>
                <input class="form-control {{ $errors->has('brand') ? 'is-invalid' : '' }}" type="text" name="brand" id="brand" value="{{ old('brand', '') }}">
                @if($errors->has('brand'))
                    <span class="text-danger">{{ $errors->first('brand') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tblHardware.fields.brand_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="outlet">{{ trans('cruds.tblHardware.fields.outlet') }}</label>
                <input class="form-control {{ $errors->has('outlet') ? 'is-invalid' : '' }}" type="text" name="outlet" id="outlet" value="{{ old('outlet', '') }}">
                @if($errors->has('outlet'))
                    <span class="text-danger">{{ $errors->first('outlet') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tblHardware.fields.outlet_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="machine">{{ trans('cruds.tblHardware.fields.machine') }}</label>
                <input class="form-control {{ $errors->has('machine') ? 'is-invalid' : '' }}" type="text" name="machine" id="machine" value="{{ old('machine', '') }}">
                @if($errors->has('machine'))
                    <span class="text-danger">{{ $errors->first('machine') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tblHardware.fields.machine_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="ftype">{{ trans('cruds.tblHardware.fields.ftype') }}</label>
                <input class="form-control {{ $errors->has('ftype') ? 'is-invalid' : '' }}" type="text" name="ftype" id="ftype" value="{{ old('ftype', '') }}">
                @if($errors->has('ftype'))
                    <span class="text-danger">{{ $errors->first('ftype') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tblHardware.fields.ftype_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection