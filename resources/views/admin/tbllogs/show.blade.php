@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.tbllog.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.tbllogs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.tbllog.fields.id') }}
                        </th>
                        <td>
                            {{ $tbllog->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tbllog.fields.id_fp') }}
                        </th>
                        <td>
                            {{ $tbllog->id_fp }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tbllog.fields.pin') }}
                        </th>
                        <td>
                            {{ $tbllog->pin }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tbllog.fields.datetime') }}
                        </th>
                        <td>
                            {{ $tbllog->datetime }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tbllog.fields.verified') }}
                        </th>
                        <td>
                            {{ $tbllog->verified }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tbllog.fields.status') }}
                        </th>
                        <td>
                            {{ $tbllog->status }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tbllog.fields.upload_status') }}
                        </th>
                        <td>
                            {{ $tbllog->upload_status }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.tbllog.fields.created_by') }}
                        </th>
                        <td>
                            {{ $tbllog->created_by }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.tbllogs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection