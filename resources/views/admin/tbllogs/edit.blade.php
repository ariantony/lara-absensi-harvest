@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.tbllog.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.tbllogs.update", [$tbllog->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="id_fp">{{ trans('cruds.tbllog.fields.id_fp') }}</label>
                <input class="form-control {{ $errors->has('id_fp') ? 'is-invalid' : '' }}" type="number" name="id_fp" id="id_fp" value="{{ old('id_fp', $tbllog->id_fp) }}" step="1">
                @if($errors->has('id_fp'))
                    <span class="text-danger">{{ $errors->first('id_fp') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tbllog.fields.id_fp_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="pin">{{ trans('cruds.tbllog.fields.pin') }}</label>
                <input class="form-control {{ $errors->has('pin') ? 'is-invalid' : '' }}" type="number" name="pin" id="pin" value="{{ old('pin', $tbllog->pin) }}" step="1">
                @if($errors->has('pin'))
                    <span class="text-danger">{{ $errors->first('pin') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tbllog.fields.pin_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="datetime">{{ trans('cruds.tbllog.fields.datetime') }}</label>
                <input class="form-control datetime {{ $errors->has('datetime') ? 'is-invalid' : '' }}" type="text" name="datetime" id="datetime" value="{{ old('datetime', $tbllog->datetime) }}">
                @if($errors->has('datetime'))
                    <span class="text-danger">{{ $errors->first('datetime') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tbllog.fields.datetime_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="verified">{{ trans('cruds.tbllog.fields.verified') }}</label>
                <input class="form-control {{ $errors->has('verified') ? 'is-invalid' : '' }}" type="number" name="verified" id="verified" value="{{ old('verified', $tbllog->verified) }}" step="1">
                @if($errors->has('verified'))
                    <span class="text-danger">{{ $errors->first('verified') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tbllog.fields.verified_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="status">{{ trans('cruds.tbllog.fields.status') }}</label>
                <input class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" type="number" name="status" id="status" value="{{ old('status', $tbllog->status) }}" step="1">
                @if($errors->has('status'))
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tbllog.fields.status_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="upload_status">{{ trans('cruds.tbllog.fields.upload_status') }}</label>
                <input class="form-control {{ $errors->has('upload_status') ? 'is-invalid' : '' }}" type="number" name="upload_status" id="upload_status" value="{{ old('upload_status', $tbllog->upload_status) }}" step="1">
                @if($errors->has('upload_status'))
                    <span class="text-danger">{{ $errors->first('upload_status') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tbllog.fields.upload_status_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="created_by">{{ trans('cruds.tbllog.fields.created_by') }}</label>
                <input class="form-control {{ $errors->has('created_by') ? 'is-invalid' : '' }}" type="text" name="created_by" id="created_by" value="{{ old('created_by', $tbllog->created_by) }}">
                @if($errors->has('created_by'))
                    <span class="text-danger">{{ $errors->first('created_by') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.tbllog.fields.created_by_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection