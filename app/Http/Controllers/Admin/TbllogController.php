<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyTbllogRequest;
use App\Http\Requests\StoreTbllogRequest;
use App\Http\Requests\UpdateTbllogRequest;
use App\Models\Tbllog;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class TbllogController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('tbllog_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Tbllog::query()->select(sprintf('%s.*', (new Tbllog())->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate = 'tbllog_show';
                $editGate = 'tbllog_edit';
                $deleteGate = 'tbllog_delete';
                $crudRoutePart = 'tbllogs';

                return view('partials.datatablesActions', compact(
                'viewGate',
                'editGate',
                'deleteGate',
                'crudRoutePart',
                'row'
            ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : '';
            });
            $table->editColumn('pin', function ($row) {
                return $row->pin ? $row->pin : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.tbllogs.index');
    }

    public function create()
    {
        abort_if(Gate::denies('tbllog_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tbllogs.create');
    }

    public function store(StoreTbllogRequest $request)
    {
        $tbllog = Tbllog::create($request->all());

        return redirect()->route('admin.tbllogs.index');
    }

    public function edit(Tbllog $tbllog)
    {
        abort_if(Gate::denies('tbllog_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tbllogs.edit', compact('tbllog'));
    }

    public function update(UpdateTbllogRequest $request, Tbllog $tbllog)
    {
        $tbllog->update($request->all());

        return redirect()->route('admin.tbllogs.index');
    }

    public function show(Tbllog $tbllog)
    {
        abort_if(Gate::denies('tbllog_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tbllogs.show', compact('tbllog'));
    }

    public function destroy(Tbllog $tbllog)
    {
        abort_if(Gate::denies('tbllog_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tbllog->delete();

        return back();
    }

    public function massDestroy(MassDestroyTbllogRequest $request)
    {
        Tbllog::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
