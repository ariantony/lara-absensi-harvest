<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyTblHardwareRequest;
use App\Http\Requests\StoreTblHardwareRequest;
use App\Http\Requests\UpdateTblHardwareRequest;
use App\Models\TblHardware;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class TblHardwareController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('tbl_hardware_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = TblHardware::query()->select(sprintf('%s.*', (new TblHardware())->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate = 'tbl_hardware_show';
                $editGate = 'tbl_hardware_edit';
                $deleteGate = 'tbl_hardware_delete';
                $crudRoutePart = 'tbl-hardwares';

                return view('partials.datatablesActions', compact(
                'viewGate',
                'editGate',
                'deleteGate',
                'crudRoutePart',
                'row'
            ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : '';
            });
            $table->editColumn('id_fp', function ($row) {
                return $row->id_fp ? $row->id_fp : '';
            });
            $table->editColumn('ip', function ($row) {
                return $row->ip ? $row->ip : '';
            });
            $table->editColumn('run_intervar', function ($row) {
                return $row->run_intervar ? $row->run_intervar : '';
            });
            $table->editColumn('outlet', function ($row) {
                return $row->outlet ? $row->outlet : '';
            });
            $table->editColumn('ftype', function ($row) {
                return $row->ftype ? $row->ftype : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.tblHardwares.index');
    }

    public function create()
    {
        abort_if(Gate::denies('tbl_hardware_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tblHardwares.create');
    }

    public function store(StoreTblHardwareRequest $request)
    {
        $tblHardware = TblHardware::create($request->all());

        return redirect()->route('admin.tbl-hardwares.index');
    }

    public function edit(TblHardware $tblHardware)
    {
        abort_if(Gate::denies('tbl_hardware_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tblHardwares.edit', compact('tblHardware'));
    }

    public function update(UpdateTblHardwareRequest $request, TblHardware $tblHardware)
    {
        $tblHardware->update($request->all());

        return redirect()->route('admin.tbl-hardwares.index');
    }

    public function show(TblHardware $tblHardware)
    {
        abort_if(Gate::denies('tbl_hardware_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.tblHardwares.show', compact('tblHardware'));
    }

    public function destroy(TblHardware $tblHardware)
    {
        abort_if(Gate::denies('tbl_hardware_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $tblHardware->delete();

        return back();
    }

    public function massDestroy(MassDestroyTblHardwareRequest $request)
    {
        TblHardware::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
