<?php

namespace App\Http\Requests;

use App\Models\Tbllog;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyTbllogRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('tbllog_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:tbllogs,id',
        ];
    }
}
