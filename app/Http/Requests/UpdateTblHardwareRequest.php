<?php

namespace App\Http\Requests;

use App\Models\TblHardware;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateTblHardwareRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('tbl_hardware_edit');
    }

    public function rules()
    {
        return [
            'id_fp' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'ip' => [
                'string',
                'nullable',
            ],
            'c_key' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'number_fp' => [
                'string',
                'nullable',
            ],
            'run_intervar' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'brand' => [
                'string',
                'nullable',
            ],
            'outlet' => [
                'string',
                'nullable',
            ],
            'machine' => [
                'string',
                'nullable',
            ],
            'ftype' => [
                'string',
                'nullable',
            ],
        ];
    }
}
