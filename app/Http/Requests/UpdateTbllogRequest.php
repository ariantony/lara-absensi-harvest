<?php

namespace App\Http\Requests;

use App\Models\Tbllog;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateTbllogRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('tbllog_edit');
    }

    public function rules()
    {
        return [
            'id_fp' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'pin' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'datetime' => [
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
                'nullable',
            ],
            'verified' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'status' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'upload_status' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'created_by' => [
                'string',
                'nullable',
            ],
        ];
    }
}
