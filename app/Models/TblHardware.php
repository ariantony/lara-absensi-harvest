<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblHardware extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'tbl_hardwares';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'id_fp',
        'ip',
        'c_key',
        'number_fp',
        'desc',
        'run_intervar',
        'brand',
        'outlet',
        'machine',
        'ftype',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
