<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbllogsTable extends Migration
{
    public function up()
    {
        Schema::create('tbllogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_fp')->nullable();
            $table->integer('pin')->nullable();
            $table->datetime('datetime')->nullable();
            $table->integer('verified')->nullable();
            $table->integer('status')->nullable();
            $table->integer('upload_status')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
