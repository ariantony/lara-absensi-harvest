<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblHardwaresTable extends Migration
{
    public function up()
    {
        Schema::create('tbl_hardwares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_fp')->nullable();
            $table->string('ip')->nullable();
            $table->integer('c_key')->nullable();
            $table->string('number_fp')->nullable();
            $table->longText('desc')->nullable();
            $table->integer('run_intervar')->nullable();
            $table->string('brand')->nullable();
            $table->string('outlet')->nullable();
            $table->string('machine')->nullable();
            $table->string('ftype')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
